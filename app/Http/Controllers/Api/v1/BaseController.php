<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    const UNAUTHENTICATED = 0;
    const ACTION_SUCCESS = 1;
    const VALIDATION_ERROR = 2;
    const ACTION_UNSUCCESSFUL = 3;
    const UNEXPECTED_ERROR = 4;
    const RESOURCE_UNAVAILABLE = 6;
    const ACTION_NOT_ALLOWED = 6;
    const ACCOUNT_SUSPENDED = 11;
    const ERROR_INVALID_OTP_ID = 12;
    const ERROR_2FA_NOT_CHECKED = 13; //Means that the user has logged in but has not submitted their OTP. For this, hit get new OTP endpoint

    const VERIFICATION_TRUE = 15; //Means phone verification is success esp on new registration verification.
    const VERIFICATION_FALSE = 16; //Opposite of 15.

    const SET_NEW_PASSWORD = 17;
}
