<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ImageUpload;
use App\Services\FileUploadService;
use App\Services\KairosService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageController extends BaseController
{
    public function image(Request $request,KairosService $kairosService)
    {

        $validator = $validator = Validator::make($request->all(), [
            "image"     => 'required | mimes:jpeg,jpg,png,bmb,gif,svg',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'response_code' => self::VALIDATION_ERROR,
                'message'       => $validator->errors()->first(),
                'errors'        => $validator->errors()
            ]);
        }

        $image = Image::make($request->image)->resize(640, 480,function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $subjectId = Str::uuid()->toString();

        $imagePath = $path = 'ai/register/face/'.$subjectId.'.png';
        $path = Storage::disk('s3')->put($imagePath, $image->stream(),'public');



        $createImage = ImageUpload::create([
            'image'=>$imagePath
        ]);

        $imagePath = env('KAIROS_FILES_BASE_URL').$imagePath;

        $enroll = $kairosService->enroll($imagePath, $subjectId, 'DemoOne');

        $success = $enroll['success'];
        $updateEnrollResponse = $createImage->update([
            'enroll_response'=>$enroll['response']
        ]);
        if (!$success) {
            return response()->json([
                'response_code' => self::ACTION_UNSUCCESSFUL,
                'message'       => $enroll['message'],
            ]);
        }



        return response()->json([
            'response_code' => self::ACTION_SUCCESS,
            'response'=>$enroll['response']
        ]);


    }
}
