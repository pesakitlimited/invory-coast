<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileUploadService
{
    public function upload($file,$folder)
    {
        $filenamewithextension = $file->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $file->getClientOriginalExtension();

        //filename to store
        $filenametostore = $folder."/".Str::uuid()->toString() . '.' . $extension;

        //Upload File to s3
        $storeImage = Storage::disk('s3')->put($filenametostore, fopen($file, 'r+'), 'public');

        return $filenametostore;


    }
}
