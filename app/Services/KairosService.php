<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class KairosService
{
    public function enroll($facePhoto, $subjectId, $gallery = 'EnrollMent')
    {
        $url = env('KAIROS_BASE_URL') . '/enroll';

        $headers = [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'app_id'       => env('KAIROS_APP_ID'),
            'app_key'      => env('KAIROS_APP_KEY')
        ];

        $data = [
            "image"        => $facePhoto,
            "subject_id"   => $subjectId,
            "gallery_name" => $gallery,
            "selector"=>"liveness"
        ];


        $response = Http::withHeaders($headers)->post($url, $data);
        if (!$response->ok()) {
            Log::critical("Data submission(Enrollment) to Kairos Failed.Response :" . $response->body());
            return [
                'success'  => false,
                'response' => null,
                'message'  => "An unexpected error occurred.Please try again."
            ];
        }

        Log::critical("Face Enrollment Response :" . $response->body());

        info($response->body());

        if (isset($response['Errors'])) {
            //Has errpr
            Log::critical("Image enrollment on enroll failed. Response : " . $response['Errors'][0]['Message']);
            $errorCode = $response['Errors'][0]['ErrCode'];

            $message = $this->getEnrollMessage((int)$errorCode);
            return [
                'success'  => false,
                'response' => $response->json(),
                'message'  => $message
            ];
        }

        $response = $response->json();
        //Check liveness
        $liveness = $response['images'][0]['attributes']['liveness'];



        if ($liveness < 0.6){
            return [
                'success'  => false,
                'response' => $response,
                'message'  => "Liveness check failed.Please provide a new photo"
            ];
        }

        return [
            'success'  => true,
            'response' => $response
        ];

    }

    private function getEnrollMessage($errorCode)
    {
        info($errorCode);
        switch ($errorCode):
//            case ($errorCode >= 1000 && $errorCode <= 3003) || 5001 || 5003 || 5004 || 5005 || 5012:
//                $message = "Photo verification failed.Please try again.";
//                break;
            case 5000:
                $message = 'Please select an image of format jpg or png.';
                break;
            case 5002:
                $message = 'Please provide and image with a face.';
                break;
            case 5010:
                $message = 'Please provide an image with only one face.';
                break;
            case 5011:
                $message = 'Photo verification failed.Please try again or contact the support team.';
                break;
            default:
                $message = "Photo verification failed.Please try again.";
                break;
        endswitch;
        return $message;
    }

    public function compare($facePhoto, $subjectId, $gallery)
    {
        $url = env('KAIROS_BASE_URL') . '/verify';

        $headers = [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'app_id'       => env('KAIROS_APP_ID'),
            'app_key'      => env('KAIROS_APP_KEY')
        ];

        $data = [
            "image"        => $facePhoto,
            "subject_id"   => $subjectId,
            "gallery_name" => $gallery
        ];


        $response = Http::withHeaders($headers)->post($url, $data);
        if (!$response->ok()) {
            Log::critical("Kairos Filed to verify .Response :" . $response->body());
            return [
                'success'  => false,
                'response' => null
            ];
        }

        Log::critical("Face verification Response :" . $response->body());


        if (isset($response['Errors'])) {
            //Has errpr
            Log::critical("Image verification on enroll failed. Response : " . $response['Errors'][0]['Message']);
            $errorCode = $response['Errors'][0]['ErrCode'];

            $message = $this->getCompareMessage((int)$errorCode);
            return [
                'success'  => false,
                'response' => null,
                'message'  => $message
            ];
        }

        $confidence = $response['images'][0]['transaction']['confidence'];

        return [
            'success'    => true,
            'response'   => $response->json(),
            'confidence' => $confidence
        ];
    }

    private function getCompareMessage($errorCode)
    {
        info("Compare :" . $errorCode);
        switch ($errorCode):
//            case ($errorCode >= 1000 && $errorCode <= 3003) || 5001 || 5003 || 5004 || 5005 || 5012:
//                $message = "Photo verification failed.Please try again.";
//                break;
            case 5000:
                $message = 'Please select an image of format jpg or png.';
                break;
            case 5002:
                $message = 'Please provide a clear image of the identification.';
                break;
            case 5010:
                $message = 'Please provide a clear image of the identification.';
                break;
            case 5011:
                $message = 'Photo verification failed.Please try again or contact the support team.';
                break;
            default:
                $message = "Photo verification failed.Please try again.";
                break;
        endswitch;
        return $message;
    }
}
